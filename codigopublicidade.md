﻿**CÓDIGO DA PUBLICIDADE**-  **PAGE 20**

verbojuridico.net

#
# ![](Aspose.Words.264add1b-2e0d-45aa-a8be-365437bd62ec.001.png "verbojuridico\_logo")
#
#
#
#
#
#
# **CÓDIGO DA PUBLICIDADE**
# ¾ Actualizado até ao Decreto-lei n.º 224/2004, de 4 de Dezembro¾


#
#
#





**Publicação**: Verbo Jurídico (www.verbojuridico.net | com | org)

Data de publicação (actualização): Março 2005.

Colaboração: Dr. Fernando Leça e Dr. António Cardoso da Conceição, Advogados.

O download deste ficheiro implica a aceitação das regras de reprodução e de direitos de autor. Na transcrição deve ser citada a respectiva fonte.
**




**CÓDIGO DA PUBLICIDADE** [^1]


**CAPÍTULO I**

**Disposições gerais**

**Artigo 1º - Âmbito do diploma**

O presente diploma aplica-se a qualquer forma de publicidade, independentemente do suporte utilizado para a sua difusão.

**Artigo 2º - Direito aplicável**

A publicidade rege-se pelo disposto no presente diploma e, subsidiariamente, pelas normas de direito civil ou comercial.

**Artigo 3º - Conceito de publicidade** [^2]

1 - Considera-se publicidade, para efeitos do presente diploma, qualquer forma de comunicação feita por entidades de natureza pública ou privada, no âmbito de uma actividade comercial, industrial, artesanal ou liberal, com o objectivo directo ou indirecto de:

a) Promover, com vista à sua comercialização ou alienação, quaisquer bens ou serviços;

b) Promover ideias, princípios, iniciativas ou instituições.

2 - Considera-se, também, publicidade qualquer forma de comunicação da Administração Pública, não prevista no número anterior, que tenha por objectivo, directo ou indirecto, promover o fornecimento de bens ou serviços.

3 - Para efeitos do presente diploma, não se considera publicidade a propaganda política.

**Artigo 4º - Conceito de actividade publicitária** [^3]

1 - Considera-se actividade publicitária o conjunto de operações relacionadas com a difusão de uma mensagem publicitária junto dos seus destinatários, bem como as relações jurídicas e técnicas daí emergentes entre anunciantes, profissionais, agências de publicidade e entidades que explorem os suportes publicitários ou que efectuem as referidas operações.

2 - Incluem-se entre as operações referidas no número anterior, designadamente, as de concepção, criação, produção, planificação e distribuição publicitárias.

**Artigo 5º - Anunciante, profissional, agência de publicidade, suporte publicitário e destinatário** [^4]

Para efeitos do disposto no presente diploma, considera-se:

a) Anunciante: a pessoa singular ou colectiva no interesse de quem se realiza a publicidade;

b) Profissional ou agência de publicidade: pessoa singular que exerce a actividade publicitária ou pessoa colectiva que tenha por objecto exclusivo o exercício da actividade publicitária;

c) Suporte publicitário: o veículo utilizado para a transmissão da mensagem publicitária;

d) Destinatário: a pessoa singular ou colectiva a quem a mensagem publicitária se dirige ou que por ela, de qualquer forma, seja atingida.

2 – Não podem constituir suporte publicitário as publicações periódicas informativas editadas pelos órgãos das autarquias locais, salvo se o anunciante for uma empresa municipal de capitais exclusiva ou maioritariamente públicos.

**CAPÍTULO II**

**Regime geral da publicidade**

**SECÇÃO I**

**Princípios gerais**

**Artigo 6º - Princípios da publicidade**

A publicidade rege-se pelos princípios da licitude, identificabilidade, veracidade e respeito pelos direitos do consumidor.

**Artigo 7º - Princípio da licitude** [^5]

1 - É proibida a publicidade que, pela sua forma, objecto ou fim, ofenda os valores, princípios e instituições fundamentais constitucionalmente consagrados.

2 - É proibida, nomeadamente, a publicidade que:

a) Se socorra, depreciativamente, de instituições, símbolos nacionais ou religiosos ou personagens históricas;

b) Estimule ou faça apelo à violência, bem como a qualquer actividade ilegal ou criminosa;

c) Atente contra a dignidade da pessoa humana;

d) Contenha qualquer discriminação em relação à raça, língua, território de origem, religião ou sexo;

e) Utilize, sem autorização da própria, a imagem ou as palavras de alguma pessoa;

f) Utilize linguagem obscena;

g) Encoraje comportamentos prejudiciais à protecção do ambiente;

h) Tenha como objecto ideias de conteúdo sindical, político ou religioso.

3 - Só é permitida a utilização de línguas de outros países na mensagem publicitária, mesmo que em conjunto com a língua portuguesa, quando aquela tenha os estrangeiros por destinatários exclusivos ou principais, sem prejuízo do disposto no número seguinte.

4 - É admitida a utilização excepcional de palavras ou de expressões em línguas de outros países quando necessárias à obtenção do efeito visado na concepção da mensagem.

**Artigo 8º - Princípio da identificabilidade** [^6]

1 - A publicidade tem de ser inequivocamente identificada como tal, qualquer que seja o meio de difusão utilizado.

2 - A publicidade efectuada na rádio e na televisão deve ser claramente separada da restante programação, através da introdução de um separador no início e no fim do espaço publicitário.

3 - O separador a que se refere o número anterior é constituído, na rádio, por sinais acústicos e, na televisão, por sinais ópticos ou acústicos, devendo, no caso da televisão, conter, de forma perceptível para os destinatários, a palavra «publicidade» no separador que precede o espaço publicitário.

**Artigo 9º - Publicidade oculta ou dissimulada**

1 - É vedado o uso de imagens subliminares ou outros meios dissimuladores que explorem a possibilidade de transmitir publicidade sem que os destinatários se apercebam da natureza publicitária da mensagem.

2 - Na transmissão televisiva ou fotográfica de quaisquer acontecimentos ou situações, reais ou simulados, é proibida a focagem directa e exclusiva da publicidade aí existente.

3 - Considera-se publicidade subliminar, para os efeitos do presente diploma, a publicidade que, mediante o recurso a qualquer técnica, possa provocar no destinatário percepções sensoriais de que ele não chegue a tomar consciência.

**Artigo 10º - Princípio da veracidade**

1 - A publicidade deve respeitar a verdade, não deformando os factos.

2 - As afirmações relativas à origem, natureza, composição, propriedades e condições de aquisição dos bens ou serviços publicitados devem ser exactas e passíveis de prova, a todo o momento, perante as instâncias competentes.

**Artigo 11º - Publicidade enganosa** [^7]

1 - É proibida toda a publicidade que, por qualquer forma, incluindo a sua apresentação, e devido ao seu carácter enganador, induza ou seja susceptível de induzir em erro os seus destinatários, independentemente de lhes causar qualquer prejuízo económico, ou que possa prejudicar um concorrente.

2 - Para se determinar se uma mensagem é enganosa devem ter-se em conta todos os seus elementos e, nomeadamente, todas as indicações que digam respeito:

a) Às características dos bens ou serviços, tais como a sua disponibilidade, natureza, execução, composição, modo e data de fabrico ou de prestação, sua adequação, utilizações, quantidade, especificações, origem geográfica ou comercial, resultados que podem ser esperados da utilização ou ainda resultados e características essenciais dos testes ou controlos efectuados sobre os bens ou serviços;

b) Ao preço e ao seu modo de fixação ou pagamento, bem como as condições de fornecimento dos bens ou da prestação dos serviços;

c) À natureza, às características e aos direitos do anunciante, tais como a sua identidade, as suas qualificações e os seus direitos de propriedade industrial, comercial ou intelectual, ou os prémios ou distinções que recebeu;

d) Aos direitos e deveres do destinatário, bem como aos termos de prestação de garantias.

3 - Considera-se, igualmente, publicidade enganosa, para efeitos do disposto no nº1, a mensagem que por qualquer forma, incluindo a sua apresentação, induza ou seja susceptível de induzir em erro o seu destinatário ao favorecer a ideia de que determinado prémio, oferta ou promoção lhe será concedido, independentemente de qualquer contrapartida económica, sorteio ou necessidade de efectuar qualquer encomenda.

4 - Nos casos previstos nos números anteriores, pode a entidade competente para a instrução dos respectivos processos de contra-ordenação exigir que o anunciante apresente provas de exactidão material dos dados de facto contidos na publicidade.

5 - Os dados referidos nos números anteriores presumem-se inexactos se as provas exigidas não forem apresentadas ou forem insuficientes.

**Artigo 12º - Princípio do respeito pelos direitos do consumidor** [^8]

É proibida a publicidade que atente contra os direitos do consumidor.

**Artigo 13º - Saúde e segurança do consumidor**

1 - É proibida a publicidade que encoraje comportamentos prejudiciais à saúde e segurança do consumidor, nomeadamente por deficiente informação acerca da perigosidade do produto ou da especial susceptibilidade da verificação de acidentes em resultado da utilização que lhe é própria.

2 - A publicidade não deve comportar qualquer apresentação visual ou descrição de situações onde a segurança não seja respeitada, salvo justificação de ordem pedagógica.

3 - O disposto nos números anteriores deve ser particularmente acautelado no caso da publicidade especialmente dirigida a crianças, adolescentes, idosos ou deficientes.

**SECÇÃO II**

**Restrições ao conteúdo da publicidade**

**Artigo 14º - Menores** [^9]

1 - A publicidade especialmente dirigida a menores deve ter sempre em conta a sua vulnerabilidade psicológica, abstendo-se, nomeadamente, de:

a) Incitar directamente os menores, explorando a sua inexperiência ou credulidade, a adquirir um determinado bem ou serviço;

b) Incitar directamente os menores a persuadirem os seus pais ou terceiros a comprarem os produtos ou serviços em questão;

c) Conter elementos susceptíveis de fazerem perigar a sua integridade física ou moral, bem como a sua saúde ou segurança, nomeadamente através de cenas de pornografia ou do incitamento à violência;

d) Explorar a confiança especial que os menores depositam nos seus pais, tutores ou professores.

2 - Os menores só podem ser intervenientes principais nas mensagens publicitárias em que se verifique existir uma relação directa entre eles e o produto ou serviço veiculado.

**Artigo 15º - Publicidade testemunhal**

A publicidade testemunhal deve integrar depoimentos personalizados, genuínos e comprováveis, ligados à experiência do depoente ou de quem ele represente, sendo admitido o depoimento despersonalizado, desde que não seja atribuído a uma testemunha especialmente qualificada, designadamente em razão do uso de uniformes, fardas ou vestimentas características de determinada profissão.

**Artigo 16º - Publicidade comparativa** [^10]

1 - É comparativa a publicidade que identifica, explícita ou implicitamente, um concorrente ou os bens ou serviços oferecidos por um concorrente.

2 - A publicidade comparativa, independentemente do suporte utilizado para a sua difusão, só é consentida, no que respeita à comparação, desde que respeite as seguintes condições:

a) Não seja enganosa, nos termos do artigo 11º;

b) Compare bens ou serviços que respondam às mesmas necessidades ou que tenham os mesmos objectivos;

c) Compare objectivamente uma ou mais características essenciais, pertinentes, comprováveis e representativas desses bens ou serviços, entre as quais se pode incluir o preço;

d) Não gere confusão no mercado entre o anunciante e um concorrente ou entre marcas, designações comerciais, outros sinais distintivos, bens ou serviços do anunciante ou de um concorrente;

e) Não desacredite ou deprecie marcas, designações comerciais, outros sinais distintivos, bens, serviços, actividades ou situação de um concorrente;

f) Se refira, em todos os casos de produtos com denominação de origem, a produtos com a mesma denominação;

g) Não retire partido indevido do renome de uma marca, designação comercial ou outro sinal distintivo de um concorrente ou da denominação de origem de produtos concorrentes;

h) Não apresente um bem ou serviço como sendo imitação ou reprodução de um bem ou serviço cuja marca ou designação comercial seja protegida.

3 - Sempre que a comparação faça referência a uma oferta especial deverá, de forma clara e inequívoca, conter a indicação do seu termo ou, se for o caso, que essa oferta especial depende da disponibilidade dos produtos ou serviços.

4 - Quando a oferta especial a que se refere o número anterior ainda não se tenha iniciado deverá indicar-se também a data de início do período durante o qual é aplicável o preço especial ou qualquer outra condição específica.

5 - O ónus da prova da veracidade da publicidade comparativa recai sobre o anunciante.

**SECÇÃO III**

**Restrições ao objecto da publicidade**

**Artigo 17º - Bebidas alcoólicas** [^11]

1 - A publicidade a bebidas alcoólicas, independentemente do suporte utilizado para a sua difusão, só é consentida quando:

a) Não se dirija especificamente a menores e, em particular, não os apresente a consumir tais bebidas;

b) Não encoraje consumos excessivos;

c) Não menospreze os não consumidores;

d) Não sugira sucesso, êxito social ou especiais aptidões por efeito do consumo;

e) Não sugira a existência, nas bebidas alcoólicas, de propriedades terapêuticas ou de efeitos estimulantes ou sedativos;

f) Não associe o consumo dessas bebidas ao exercício físico ou à condução de veículos;

g) Não sublinhe o teor de álcool das bebidas como qualidade positiva.

2 - É proibida a publicidade a bebidas alcoólicas, na televisão e na rádio, entre as 7 horas e as 22 horas e 30 minutos.

3 - Para efeitos do disposto no número anterior é considerada a hora oficial do local de origem da emissão.

4 - Sem prejuízo do disposto na alínea a) do nº2 do artigo 7º, é proibido associar a publicidade de bebidas alcoólicas aos símbolos nacionais, consagrados no artigo 11º da Constituição da República Portuguesa.

5 - As comunicações comerciais e a publicidade de quaisquer eventos em que participem menores, designadamente actividades desportivas, culturais, recreativas ou outras, não devem exibir ou fazer qualquer menção, implícita ou explícita, a marca ou marcas de bebidas alcoólicas.

6 - Nos locais onde decorram os eventos referidos no número anterior não podem ser exibidas ou de alguma forma publicitadas marcas de bebidas alcoólicas.

**Artigo 18º - Tabaco** [^12]

São proibidas, sem prejuízo do disposto em legislação especial, todas as formas de publicidade ao tabaco através de suportes sob a jurisdição do Estado Português.

**Artigo 19º - Tratamentos e medicamentos**

É proibida a publicidade a tratamentos médicos e a medicamentos que apenas possam ser obtidos mediante receita médica, com excepção da publicidade incluída em publicações técnicas destinadas a médicos e outros profissionais de saúde.

**Artigo 20º - Publicidade em estabelecimentos de ensino ou destinada a menores** [^13]

É proibida a publicidade a bebidas alcoólicas, ao tabaco ou a qualquer tipo de material pornográfico em estabelecimentos de ensino, bem como em quaisquer publicações, programas ou actividades especialmente destinados a menores.

**Artigo 21º - Jogos de fortuna ou azar**

1 - Não podem ser objecto de publicidade os jogos de fortuna ou azar enquanto objecto essencial da mensagem.

2 - Exceptuam-se do disposto no número anterior os jogos promovidos pela Santa Casa da Misericórdia de Lisboa.

**Artigo 22º - Cursos** [^14]

A mensagem publicitária relativa a cursos ou quaisquer outras acções de formação ou aperfeiçoamento intelectual, cultural ou profissional deve indicar:

a) A natureza desses cursos ou acções, de acordo com a designação oficialmente aceite pelos serviços competentes, bem como a duração dos mesmos;

b) A expressão «sem reconhecimento oficial», sempre que este não tenha sido atribuído pelas entidades oficiais competentes.

**Artigo 22º-A - Veículos automóveis** [^15] [^16]

1 - É proibida a publicidade a veículos automóveis que:

a) Contenha situações ou sugestões de utilização do veículo que possam pôr em risco a segurança pessoal do utente ou de terceiros;

b) Contenha situações ou sugestões de utilização do veículo perturbadoras do meio ambiente;

c) Apresente situações de infracção das regras do Código da Estrada, nomeadamente excesso de velocidade, manobras perigosas, não utilização de acessórios de segurança e desrespeito pela sinalização ou pelos peões.

2 - Para efeitos do presente Código, entende-se por veículos automóveis todos os veículos de tracção mecânica destinados a transitar pelos seus próprios meios nas vias públicas.

**Artigo 22º-B - Produtos e serviços milagrosos** [^17]

1 - É proibida, sem prejuízo do disposto em legislação especial, a publicidade a bens ou serviços milagrosos.

2 - Considera-se publicidade a bens ou serviços milagrosos, para efeitos do presente diploma, a publicidade que, explorando a ignorância, o medo, a crença ou a superstição dos destinatários, apresente quaisquer bens, produtos, objectos, aparelhos, materiais, substâncias, métodos ou serviços como tendo efeitos específicos automáticos ou garantidos na saúde, bem-estar, sorte ou felicidade dos consumidores ou de terceiros, nomeadamente por permitirem prevenir, diagnosticar, curar ou tratar doenças ou dores, proporcionar vantagens de ordem profissional, económica ou social, bem como alterar as características físicas ou a aparência das pessoas, sem uma objectiva comprovação científica das propriedades, características ou efeitos propagandeados ou sugeridos.

3 - O ónus da comprovação científica a que se refere o número anterior recai sobre o anunciante.

4 - As entidades competentes para a instrução dos processos de contra-ordenação e para a aplicação das medidas cautelares e das coimas previstas no presente diploma podem exigir que o anunciante apresente provas da comprovação científica a que se refere o nº2, bem como da exactidão material dos dados de facto e de todos os benefícios propagandeados ou sugeridos na publicidade.

5 - A comprovação científica a que se refere o nº2 bem como os dados de facto e os benefícios a que se refere o número anterior presumem-se inexistentes ou inexactos se as provas exigidas não forem imediatamente apresentadas ou forem insuficientes.

**SECÇÃO IV**

**Formas especiais de publicidade**

**Artigo 23º - Publicidade domiciliária e por correspondência** [^18]

1 - Sem prejuízo do disposto em legislação especial, a publicidade entregue no domicílio do destinatário, por correspondência ou qualquer outro meio, deve conter, de forma clara e precisa:

a) O nome, domicílio e os demais elementos necessários para a identificação do anunciante;

b) A indicação do local onde o destinatário pode obter as informações de que careça;

c) A descrição rigorosa e fiel do bem ou serviço publicitado e das suas características;

d) O preço do bem ou serviço e a respectiva forma de pagamento, bem como as condições de aquisição, de garantia e de assistência pós-venda.

2 - Para efeitos das alíneas a) e b) do número anterior, não é admitida a indicação, em exclusivo, de um apartado ou qualquer outra menção que não permita a localização imediata do anunciante.

3 - A publicidade indicada no nº1 só pode referir-se a artigos de que existam amostras disponíveis para exame do destinatário.

4 - O destinatário da publicidade abrangida pelo disposto nos números anteriores não é obrigado a adquirir, guardar ou devolver quaisquer bens ou amostras que lhe tenham sido enviados ou entregues à revelia de solicitação sua.

**Artigo 24º - Patrocínio** [^19]

1 - Entende-se por patrocínio, para efeitos do presente diploma, a participação de pessoas singulares ou colectivas que não exerçam a actividade televisiva ou de produção de obras áudio-visuais no financiamento de quaisquer obras áudio-visuais, programas, reportagens, edições, rubricas ou secções, adiante designados abreviadamente por programas, independentemente do meio utilizado para a sua difusão, com vista à promoção do seu nome, marca ou imagem, bem como das suas actividades, bens ou serviços.

2 - Os programas televisivos não podem ser patrocinados por pessoas singulares ou colectivas que tenham por actividade principal o fabrico ou a venda de cigarros ou de outros produtos derivados do tabaco.

3 - Os telejornais e os programas televisivos de informação política não podem ser patrocinados.

4 - Os programas patrocinados devem ser claramente identificados como tal pela indicação do nome ou logótipo do patrocinador no início e, ou, no final do programa, sem prejuízo de tal indicação poder ser feita, cumulativamente, noutros momentos, de acordo com o regime previsto no artigo 25º para a inserção de publicidade na televisão.

5 - O conteúdo e a programação de uma emissão patrocinada não podem, em caso algum, ser influenciados pelo patrocinador, por forma a afectar a responsabilidade e a independência editorial do emissor.

6 - Os programas patrocinados não podem incitar à compra ou locação dos bens ou serviços do patrocinador ou de terceiros, especialmente através de referências promocionais específicas a tais bens ou serviços.


**CAPÍTULO III** [^20]

**Publicidade na televisão e televenda**

**Artigo 25º - Inserção da publicidade na televisão** [^21]

1 - A publicidade televisiva deve ser inserida entre programas.

2 - A publicidade só pode ser inserida durante os programas, desde que não atente contra a sua integridade e tenha em conta as suas interrupções naturais, bem como a sua duração e natureza, e de forma a não lesar os direitos de quaisquer titulares.

3 - A publicidade não pode ser inserida durante a transmissão de serviços religiosos.

4 - Os telejornais, os programas de informação política, os programas de actualidade informativa, as revistas de actualidade, os documentários, os programas religiosos e os programas para crianças com duração programada inferior a trinta minutos não podem ser interrompidos por publicidade.

5 - Nos programas compostos por partes autónomas, nas emissões desportivas e nas manifestações ou espectáculos de estrutura semelhante, que compreendam intervalos, a publicidade só pode ser inserida entre aquelas partes autónomas ou nos intervalos.

6 - Sem prejuízo do disposto no número anterior, entre duas interrupções sucessivas do mesmo programa, para emissão de publicidade, deve mediar um período igual ou superior a vinte minutos.

7 - A transmissão de obras áudio-visuais com duração programada superior a quarenta e cinco minutos, designadamente longas metragens cinematográficas e filmes concebidos para a televisão, com excepção de séries, folhetins, programas de diversão e documentários, só pode ser interrompida uma vez por cada período completo de quarenta e cinco minutos, sendo admitida outra interrupção se a duração programada da transmissão exceder em, pelo menos, vinte minutos dois ou mais períodos completos de quarenta e cinco minutos.

8 - As mensagens publicitárias isoladas só podem ser inseridas a título excepcional.

9 - Para efeitos do disposto no presente artigo, entende-se por duração programada de um programa o tempo efectivo do mesmo, descontando o período dedicado às interrupções, publicitárias e outras.

**Artigo 25º-A – Televenda** [^22]

1 - Considera-se televenda, para efeitos do presente diploma, a difusão de ofertas directas ao público, realizada por canais televisivos, com vista ao fornecimento de produtos ou à prestação de serviços, incluindo bens imóveis, direitos e obrigações mediante remuneração.

2 - São aplicáveis à televenda, com as necessárias adaptações, as disposições previstas neste Código para a publicidade, sem prejuízo do disposto nos números seguintes.

3 - É proibida a televenda de medicamentos sujeitos a uma autorização de comercialização, assim como a televenda de tratamentos médicos.

4 - A televenda não deve incitar os menores a contratarem a compra ou aluguer de quaisquer bens ou serviços.

**Artigo 26º - Tempo reservado à publicidade** [^23]

**CAPÍTULO IV**

**Actividade publicitária**

**SECÇÃO I**

**Publicidade do Estado**

**Artigo 27º - Publicidade do Estado** [^24]

A publicidade do Estado é regulada em diploma próprio.

**SECÇÃO II**

**Relações entre sujeitos da actividade publicitária**

**Artigo 28º - Respeito pelos fins contratuais**

É proibida a utilização para fins diferentes dos acordados de qualquer ideia, informação ou material publicitário fornecido para fins contratuais relacionados com alguma ou algumas das operações referidas no nº2 do artigo 4º.

**Artigo 29º - Criação publicitária**

1 - As disposições legais sobre direitos de autor aplicam-se à criação publicitária, sem prejuízo do disposto nos números seguintes.

2 - Os direitos de carácter patrimonial sobre a criação publicitária presumem-se, salvo convenção em contrário, cedidos em exclusivo ao seu criador intelectual.

3 - É ilícita a utilização de criações publicitárias sem a autorização dos titulares dos respectivos direitos.

**Artigo 30º - Responsabilidade civil** [^25]

1 - Os anunciantes, os profissionais, as agências de publicidade e quaisquer outras entidades que exerçam a actividade publicitária, bem como os titulares dos suportes publicitários utilizados ou os respectivos concessionários, respondem civil e solidariamente, nos termos gerais, pelos prejuízos causados a terceiros em resultado da difusão de mensagens publicitárias ilícitas.

2 - Os anunciantes eximir-se-ão da responsabilidade prevista no número anterior caso provem não ter tido prévio conhecimento da mensagem publicitária veiculada.

**CAPÍTULO V**

**Conselho Consultivo da Actividade Publicitária**

**Artigo 31º - Natureza e funções** [^26]

**Artigo 32º - Composição** [^27]

**Artigo 33º - Funcionamento** [^28]

**CAPÍTULO VI**

**Fiscalização e sanções**

**Artigo 34º - Sanções** [^29]

1 - A infracção ao disposto no presente diploma constitui contra-ordenação punível com as seguintes coimas:

a) De 350.000$00 a 750.000$00 ou de 700.000$00 a 9.000.000$00, consoante o infractor seja pessoa singular ou colectiva, por violação do preceituado nos artigos 7º, 8º, 9º, 10º, 11º, 12º, 13º, 14º, 16º, 20º, 22º-B, 23º, 24º, 25º e 25º-A;

b) De 200.000$00 a 700.000$00 ou de 500.000$00 a 5.000.000$00, consoante o infractor seja pessoa singular ou colectiva, por violação do preceituado nos artigos 17º, 18º e 19º;

c) De 75.000$00 a 500.000$00 ou de 300.000$00 a 1.600.000$00, consoante o infractor seja pessoa singular ou colectiva, por violação do preceituado nos artigos 15º, 21º, 22º e 22º-A.

2 - A negligência é sempre punível, nos termos gerais.

**Artigo 35º - Sanções acessórias** [^30]

1 - Sem prejuízo do disposto no artigo anterior, podem ainda ser aplicadas as seguintes sanções acessórias:

a) Apreensão de objectos utilizados na prática das contra-ordenações;

b) Interdição temporária, até um máximo de dois anos, de exercer a actividade publicitária;

c) Privação do direito a subsídio ou benefício outorgado por entidades ou serviços públicos;

d) Encerramento temporário das instalações ou estabelecimentos onde se verifique o exercício da actividade publicitária, bem como cancelamento de licenças ou alvarás.

2 - As sanções acessórias previstas nas alíneas b), c) e d) do número anterior só podem ser aplicadas em caso de dolo na prática das correspondentes infracções.

3 - As sanções acessórias previstas nas alíneas c) e d) do nº1 têm a duração máxima de dois anos.

4 - Em casos graves ou socialmente relevantes pode a entidade competente para decidir da aplicação da coima ou das sanções acessórias determinar a publicidade da punição por contra-ordenação, a expensas do infractor.

**Artigo 36º - Responsabilidade pela contra-ordenação** [^31]

São punidos como agentes das contra-ordenações previstas no presente diploma o anunciante, o profissional, a agência de publicidade ou qualquer outra entidade que exerça a actividade publicitária, o titular do suporte publicitário ou o respectivo concessionário, bem como qualquer outro interveniente na emissão da mensagem publicitária.

**Artigo 37º - Fiscalização** [^32]

Sem prejuízo da competência das autoridades policiais e administrativas, compete especialmente ao Instituto do Consumidor a fiscalização do cumprimento do disposto no presente diploma, devendo-lhe ser remetidos os autos de notícia levantados ou as denúncias recebidas.

**Artigo 38º - Instrução dos processos** [^33]

A instrução dos processos pelas contra-ordenações previstas neste diploma compete ao Instituto do Consumidor.

**Artigo 39º - Aplicação de sanções** [^34] [^35]

1 - A aplicação das coimas previstas no presente diploma compete a uma comissão constituída pelos seguintes membros:

a) O presidente da comissão referida no nº2 do artigo 52º do Decreto-Lei nº28/84, de 20 de Janeiro, que presidirá;

b) O presidente do Instituto do Consumidor;

c) O presidente do Instituto da Comunicação Social.

2 - À comissão mencionada no número anterior aplica-se, com as devidas adaptações, o Decreto-Lei nº214/84, de 3 de Julho, sendo apoiada pelo Instituto do Consumidor.

3 - Sempre que a comissão entenda que conjuntamente com a coima é de aplicar alguma das sanções acessórias previstas no presente diploma, remeterá o respectivo processo, acompanhado de proposta fundamentada, ao membro do Governo que tenha a seu cargo a tutela da protecção do consumidor, ao qual compete decidir das sanções acessórias propostas.

4 - Sem prejuízo do disposto no número seguinte, as receitas das coimas revertem:

a) Em 20% para a entidade autuante;

b) Em 20% para o Instituto do Consumidor;

c) Em 60% para o Estado.

5 - As receitas das coimas aplicadas por infracção ao disposto no artigo 17º revertem:

a) Em 20% para a entidade autuante;

b) Em 20% para o Instituto do Consumidor;

c) Em 60% para um fundo destinado a financiar campanhas de promoção e educação para a saúde e o desenvolvimento de medidas de investigação, prevenção, tratamento e reabilitação dos problemas relacionados com o álcool.

**Artigo 40º - Regras especiais sobre competências** [^36]

1 - A fiscalização do cumprimento do disposto no artigo 19º, bem como a instrução dos respectivos processos de contra-ordenação e a aplicação das correspondentes coimas e sanções acessórias, competem à Direcção-Geral dos Cuidados de Saúde Primários, à Direcção-Geral dos Assuntos Farmacêuticos e aos respectivos serviços competentes nas Regiões Autónomas dos Açores e da Madeira.

2 - A fiscalização do cumprimento do disposto no artigo 24º na actividade de televisão e, bem assim, nos artigos 25º e 25º-A, a instrução dos respectivos processos e a aplicação das correspondentes coimas e sanções acessórias competem à entidade administrativa independente reguladora da comunicação social.

3 - As receitas das coimas aplicadas ao abrigo do disposto nos números anteriores revertem em 40% para a entidade instrutora e em 60% para o Estado.

**Artigo 41º - Medidas cautelares** [^37]

1 - Em caso de publicidade enganosa, publicidade comparativa ilícita ou de publicidade que, pelo seu objecto, forma ou fim, acarrete ou possa acarretar riscos para a saúde, a segurança, os direitos ou os interesses legalmente protegidos dos seus destinatários, de menores ou do público a entidade competente para a aplicação das coimas previstas no presente diploma, sob proposta das entidades com competência para a fiscalização das infracções em matéria de publicidade, pode ordenar medidas cautelares de suspensão, cessação ou proibição daquela publicidade, independentemente de culpa ou da prova de uma perda ou de um prejuízo real.

2 - A adopção das medidas cautelares a que se refere o número anterior deve, sempre que possível, ser precedida da audição do anunciante, do titular ou do concessionário do suporte publicitário, conforme os casos, que dispõem para o efeito do prazo de três dias úteis.

3 - A entidade competente para ordenar a medida cautelar pode exigir que lhe sejam apresentadas provas de exactidão material dos dados de facto contidos na publicidade, nos termos do disposto nos ns.4 e 5 do artigo 11º.

4 - A entidade competente para ordenar a medida cautelar pode conceder um prazo para que sejam suprimidos os elementos ilícitos da publicidade.

5 - O acto que aplique a medida cautelar de suspensão da publicidade terá de fixar expressamente a sua duração, que não poderá ultrapassar os 60 dias.

6 - O acto que aplique as medidas cautelares a que se refere o nº1 poderá determinar a sua publicitação, a expensas do anunciante, do titular ou do concessionário do suporte publicitário, conforme os casos, fixando os termos da respectiva difusão.

7 - Quando a gravidade do caso o justifique ou daí possa resultar a minimização dos efeitos da publicidade ilícita, pode a entidade referida no nº1 ordenar ao anunciante, ao titular ou ao concessionário do suporte publicitário, conforme os casos, a difusão, a expensas suas, de publicidade correctora, determinando os termos da respectiva difusão.

8 - Do acto que ordena a aplicação das medidas cautelares a que se refere o nº1 cabe recurso, nos termos da lei geral.

9 - O regime previsto no presente artigo também se aplica à publicidade de ideias de conteúdo político ou religioso.



[^1]: Aprovado pelo art.1º do DL 330/90, de 23/10, alterado pelo art.1º do DL 74/93, de 10/03, pelos arts.1º, 2º e 4º do DL 6/95, de 17/01, pelo artigo único do DL 61/97, de 25/03, pelo art.75º da Lei 31-A/98, de 14/07, pelo DL 275/98, de 09/09, cujo art.8º o republicou na íntegra, pelo art.1º do DL 51/2001, de 15/02, pelo art.1º do DL 332/2001, de 24/12, pelo DL 81/2002, de 04/04, pelo art.91º da Lei 32/2003, de 22/08 e pelo Dec.-Lei n.º 224/2004, de 4/12.
[^2]: Este artigo tem a redacção do art.1º do DL 6/95, de 17/01.
[^3]: Este artigo tem a redacção do art.1º do DL 275/98, de 09/09.
[^4]: Este artigo tem a redacção do art.1º do DL 275/98, de 09/09. O número 2 foi introduzido pelo artigo único do DL 224/2004, de 04/12.
[^5]: Este artigo tem a redacção do art.1º do DL 275/98, de 09/09; este artigo tem a redacção do art.1º do DL 6/95, de 17/01.
[^6]: Este artigo tem a redacção do art.1º do DL 6/95, de 17/01.
[^7]: Este artigo tem a redacção do art.1º do DL 275/98, de 09/09.
[^8]: Este artigo tem a redacção do art.1º do DL 275/98, de 09/09.
[^9]: Este artigo tem a redacção do art.1º do DL 275/98, de 09/09.
[^10]: Este artigo tem a redacção do art.1º do DL 275/98, de 09/09.
[^11]: Este artigo tem a redacção do art.1º do DL 332/2001, de 24/12; este artigo tem a redacção do art.1º do DL 51/2001, de 15/02.
[^12]: Este artigo tem a redacção do art.1º do DL 275/98, de 09/09.
[^13]: Este artigo tem a redacção do art.1º do DL 275/98, de 09/09.
[^14]: Este artigo tem a redacção do art.1º do DL 275/98, de 09/09.
[^15]: Relativamente à venda de automóveis o art.2º do DL 74/93, de 10/03 estabelece o seguinte, cuja infracção constitui contra-ordenação: “Artigo 2º - 1 - Na venda de automóveis ligeiros de passageiros e motociclos usados é obrigatória a prestação das seguintes informações:

    a) Matrícula;

    b) Preço;

    c) Ano de construção, conforme o respectivo livrete;

    d) Data de matrícula, conforme o respectivo livrete;

    e) Registos anteriores de propriedade e seu número, conforme o respectivo título;

    f) Garantia de fábrica: prazo de garantia e quilómetros, ou qualquer outra garantia dada pelo fabricante, cuja validade ainda não tenha expirado;

    g) Garantia de usado: prazo ou quilómetros, ou outra garantia que o vendedor conceda.

    2 - Na venda de ciclomotores usados é obrigatória a prestação das informações previstas nas alíneas a) a d) e f) e g) do número anterior.

    3 - Exceptua-se do disposto nos números anteriores a venda feita directamente pelo proprietário indicado no título de registo de propriedade ou, no caso dos ciclomotores, no certificado de matrícula, quando actue fora do exercício do comércio.

    4 - As informações previstas nos ns.1 e 2 constarão obrigatoriamente de documento escrito, assinado pelo vendedor ou intermediário, que será afixado no veículo, de modo visível, de forma a permitir uma fácil leitura pelo interessado, sendo o respectivo duplicado entregue ao comprador no momento da compra e venda.”
[^16]: Este artigo foi aditado pelo art.1º do DL 74/93, de 10/03.
[^17]: Este artigo foi aditado pelo art.2º do DL 275/98, de 09/09.
[^18]: Este artigo tem a redacção do art.1º do DL 275/98, de 09/09.
[^19]: Este artigo tem a redacção do art.1º do DL 275/98, de 09/09.
[^20]: Este capítulo tem a redacção do art.1º do DL 275/98, de 09/09.
[^21]: Este artigo tem a redacção do art.1º do DL 275/98, de 09/09; este artigo tem a redacção do art.1º do DL 6/95, de 17/01.
[^22]: Este artigo foi aditado pelo art.2º do DL 275/98, de 09/09.
[^23]: Este artigo foi revogado pelo nº2 do art.75º da Lei 31-A/98, de 14/07; este artigo tem a redacção do art.1º do DL 6/95, de 17/01.
[^24]: Este artigo tem a redacção do art.1º do DL 275/98, de 09/09; este artigo tem a redacção do art.1º do DL 6/95, de 17/01; este artigo tem a redacção do DL 224/2004, de 04/12.
[^25]: Este artigo tem a redacção do art.1º do DL 275/98, de 09/09.
[^26]: Este artigo foi revogado pelo art.4º do DL 6/95, de 17/01.
[^27]: Este artigo foi revogado pelo art.4º do DL 6/95, de 17/01.
[^28]: Este artigo foi revogado pelo art.4º do DL 6/95, de 17/01.
[^29]: Este artigo tem a redacção do art.1º do DL 275/98, de 09/09; este artigo foi alterado pelo art.1º do DL 74/93, de 10/03.
[^30]: Este artigo tem a redacção do art.1º do DL 275/98, de 09/09.
[^31]: Este artigo tem a redacção do art.1º do DL 275/98, de 09/09.
[^32]: Este artigo tem a redacção do art.1º do DL 6/95, de 17/01.
[^33]: Este artigo tem a redacção do art.1º do DL 275/98, de 09/09; este artigo tem a redacção do art.1º do DL 6/95, de 17/01.
[^34]: Este artigo tem a redacção do art.1º do DL 332/2001, de 24/12; este artigo tem a redacção do art.1º do DL 275/98, de 09/09; este artigo tem a redacção do art.1º do DL 6/95, de 17/01.
[^35]: A Comissão de Aplicação de Coimas em Matéria Económica e Publicidade foi regulamentada pelo DL 81/2002, de 04/04.
[^36]: Este artigo tem a redacção do art.91º da Lei 32/2003, de 22/08.
[^37]: Este artigo tem a redacção do art.1º do DL 275/98, de 09/09; este artigo foi aditado pelo art.2º do DL 6/95, de 17/01.